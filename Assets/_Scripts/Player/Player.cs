﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    InteractiveObject iObj;
    public bool hasKey = false;


    // Update is called once per frame
    void Update () {
	    if(iObj != null && Input.GetKeyDown(KeyCode.E))
        {
            iObj.OnInteract();
        }
	}

    public void OnTriggerEnter2D(Collider2D coll)
    {
        var newIObj = coll.GetComponent<InteractiveObject>();
        if (newIObj != null)
        {
            iObj = newIObj;
        }
    }
    public void OnTriggerExit2D(Collider2D coll)
    {
        var newIObj = coll.GetComponent<InteractiveObject>();
        if (newIObj == iObj)
        {
            iObj = null;
        }
    }

}
