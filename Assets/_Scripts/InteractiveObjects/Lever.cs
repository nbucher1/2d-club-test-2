﻿using UnityEngine;
using System.Collections;
using System;

public class Lever : InteractiveObject {
    Animator a;

    // Use this for initialization
    void Start () {
        a = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public override void OnInteract()
    {
        Debug.Log("interacting");
        var oldVal = a.GetBool("LeverOn");
        a.SetBool("LeverOn", !oldVal);
    }

}
