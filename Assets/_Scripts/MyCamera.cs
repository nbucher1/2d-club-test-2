﻿using UnityEngine;
using System.Collections;


public class MyCamera : MonoBehaviour
{
    public GameObject player;
    Vector3 oldPlayerPos;


    // Use this for initialization
    void Start()
    {
        oldPlayerPos = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayerBox(3, 6);
        //FollowPlayerExactly();
        oldPlayerPos = player.transform.position;
    }

    void FollowPlayerExactly()
    {
        this.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            transform.position.z);
    }

    void FollowPlayerBox(float width, float height)
    {
        var pPosDiff = new Vector3();
        //check to see if player is out of bounds
        if(player.transform.position.x >= this.transform.position.x + (width/2.0f) ||
            player.transform.position.x <= this.transform.position.x - (width / 2.0f))
        {
            //get the difference between the player position and the old player position
            pPosDiff.x = player.transform.position.x - oldPlayerPos.x;
        }
        if (player.transform.position.y >= this.transform.position.y + (height / 2.0f) ||
             player.transform.position.y <= this.transform.position.y - (height / 2.0f))
        {
            //get the difference between the player position and the old player position
            pPosDiff.y = player.transform.position.y - oldPlayerPos.y;
        }
        //update the camera's position
        transform.position = transform.position + pPosDiff;
    }



    public IEnumerator Shake(float duration, float magnitude)
    {

        float elapsed = 0.0f;

        Vector3 originalCamPos = Camera.main.transform.position;

        while (elapsed < duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            transform.position = player.transform.position + new Vector3(x, y, originalCamPos.z);

            yield return null;
        }

        Camera.main.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
    }
}
