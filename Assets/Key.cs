﻿using UnityEngine;
using System.Collections;
using System;

public class Key : InteractiveObject {
    public override void OnInteract()
    {
        var p = GameObject.Find("Player").GetComponent<Player>();
        p.hasKey = true;
        Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
