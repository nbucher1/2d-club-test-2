﻿using UnityEngine;
using System.Collections;
using System;

public class Door : InteractiveObject {
    public override void OnInteract()
    {
        var p = GameObject.Find("Player").GetComponent<Player>();
        if(p.hasKey)
        {
            var a = GetComponent<Animator>();
            a.SetBool("Open", true);
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
