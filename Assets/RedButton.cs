﻿using UnityEngine;
using System.Collections;
using System;

public class RedButton : InteractiveObject {
    public override void OnInteract()
    {
        var m = transform.Find("Sprite").Find("Box").GetComponent<SpriteRenderer>().material;
        if(m.color == Color.red)
        {
            m.color = Color.yellow;
        }
        else
        {
            m.color = Color.red;
        }
        var c = GameObject.Find("Main Camera").GetComponent<MyCamera>();
        StartCoroutine(c.Shake(2, .2f));
    }

    // Use this for initialization
    void Start () {
        var m = transform.Find("Sprite").Find("Box").GetComponent<SpriteRenderer>().material;
        m.color = Color.red;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
